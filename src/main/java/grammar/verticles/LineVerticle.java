package grammar.verticles;

import grammar.objects.Line;
import grammar.objects.Objects;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;

public class LineVerticle extends AbstractVerticle {

	public Objects languageObject;
	
	
	
	@Override
	public void start() throws Exception {
		   // Do something
		EventBus eb = vertx.eventBus();
		eb.consumer("news.uk.sport", message -> {
			  System.out.println("I LineVerticle have received a message: " + message.body());
			  
			  languageObject = new Line();
			  
			  if(message.body().toString().toLowerCase().contains("line")) {
			  String action = languageObject.addWordAndReturnAction();
			  
			  
				  	eb.publish("news.uk.sport", "Sending to "+action);
			  }
			});
		
		 }
	
	@Override
		 public void stop(Promise<Void> stopPromise) {
			 
			 
		 }


}
