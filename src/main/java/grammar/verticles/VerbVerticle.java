package grammar.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

public class VerbVerticle extends AbstractVerticle {
	
	@Override
	public void start() {
		   // Do something
		EventBus eb = vertx.eventBus();
		eb.consumer("news.uk.sport", message -> {
			  System.out.println("I have received a message: " + message.body());
			  
			
			  
			  if(message.body().toString().toLowerCase().contains("verb"))
				  	eb.publish("news.uk.sport", "Sending to Noun");
			});
		
		 }
	
	
	@Override
	public void stop(Promise<Void> stopPromise) {
			 
			 
		 }

}
