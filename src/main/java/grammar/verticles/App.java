package grammar.verticles;

import io.vertx.core.Verticle;
import io.vertx.core.Vertx;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("MEHHH");
		Vertx vertx = Vertx.vertx();
		
		Verticle nounVerticle = new NounVerticle();
		vertx.deployVerticle(nounVerticle);
		
		Verticle verbVerticle = new VerbVerticle();
		vertx.deployVerticle(verbVerticle);
		
		Verticle adjectiveVerticle = new AdjectiveVerticle();
		vertx.deployVerticle(adjectiveVerticle);
		
		Verticle prepositionVerticle = new PrepositionVerticle();
		vertx.deployVerticle(prepositionVerticle);
		
		Verticle pronounVerticle = new PronounVerticle();
		vertx.deployVerticle(pronounVerticle);
		
		
		Verticle lineVerticle = new LineVerticle();
		
		
		vertx.deployVerticle(lineVerticle, res-> {
            if (res.succeeded()) {
                System.out.println("lineVerticle deployed");

                

      vertx.eventBus().publish("news.uk.sport", "main sent line");
            }
		});
		
		
	}

}
