package grammar.verticles;

import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import grammar.objects.Pronoun;
import grammar.objects.Objects;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;

public class PronounVerticle extends AbstractVerticle{
	
	public Objects languageObject;
	
	
	
	@Override
	public void start() throws Exception {
		   // Do something
		EventBus eb = vertx.eventBus();
		languageObject = new Pronoun();
		eb.consumer("news.uk.sport", message -> {
			  System.out.println("I Pronoun received a message: " + message.body());
			  
			 if(message.body().toString().toLowerCase().contains("pronoun")) {
				  
				  String action = languageObject.addWordAndReturnAction();
				  
				 eb.publish("news.uk.sport", "Sending to "+action);
				  }
				  	});
		
		 }
	
	@Override
		 public void stop(Promise<Void> stopPromise) {
			 
			 
		 }

}
