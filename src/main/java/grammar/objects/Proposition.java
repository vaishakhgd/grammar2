package grammar.objects;

import java.util.Random;

import grammar.constants.*;
import grammar.io.Output;

public class Proposition  implements Objects{
	
	
	
	
	public void printOutputLine() {
		System.out.println("----------------------PREPOSITION ADDED---------------------------------");
		System.out.println(Output.line);
		System.out.println("---------------------PREPOSITION ADDED----------------------------------");
	}
	
	

	@Override
	public String addWordAndReturnAction() {
		
		
		String randomWord = Preposition.words[(int) (Math.random() * Preposition.words.length)];
		
		Output.line += " " + randomWord +" ";
		
		printOutputLine();
		
		String randomAction = Preposition.actions[(int) (Math.random() * Preposition.actions.length)];
		
		return randomAction;
			
	}

}

