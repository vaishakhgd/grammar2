package grammar.objects;


import grammar.io.Output;

public class Pronoun  implements Objects{
	
	
	
	
	public void printOutputLine() {
		System.out.println("----------------------PRoNOUN ADDED---------------------------------");
		System.out.println(Output.line);
		System.out.println("---------------------PRoNOUN ADDED----------------------------------");
	}
	
	

	@Override
	public String addWordAndReturnAction() {
		
		
		String randomWord = grammar.constants.Pronoun.words[(int) (Math.random() * grammar.constants.Pronoun.words.length)];
		
		Output.line += " " + randomWord +" ";
		
		printOutputLine();
		
		String randomAction = grammar.constants.Pronoun.actions[(int) (Math.random() * grammar.constants.Pronoun.actions.length)];
		
		return randomAction;
			
	}

}

