package grammar.objects;


import grammar.io.Output;

public class Adjective  implements Objects{
	
	
	
	
	public void printOutputAdjective() {
		System.out.println("----------------------Adjective ADDED---------------------------------");
		System.out.println(Output.line);
		System.out.println("---------------------Adjective ADDED----------------------------------");
	}
	
	

	@Override
	public String addWordAndReturnAction() {
		
		
		String randomWord = grammar.constants.Adjective.words[(int) (Math.random() * grammar.constants.Adjective.words.length)];
		
		Output.line += " " + randomWord +" ";
		
		printOutputAdjective();
		
		String randomAction = grammar.constants.Adjective.actions[(int) (Math.random() * grammar.constants.Adjective.actions.length)];
		
		return randomAction;
			
	}

}

