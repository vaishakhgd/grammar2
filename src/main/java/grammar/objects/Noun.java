package grammar.objects;


import grammar.io.Output;

public class Noun  implements Objects{
	
	
	
	
	public void printOutputLine() {
		System.out.println("----------------------Noun ADDED---------------------------------");
		System.out.println(Output.line);
		System.out.println("---------------------Noun ADDED----------------------------------");
	}
	
	

	@Override
	public String addWordAndReturnAction() {
		
		
		String randomWord = grammar.constants.Noun.words[(int) (Math.random() * grammar.constants.Noun.words.length)];
		
		Output.line += " " + randomWord +" ";
		
		printOutputLine();
		
		String randomAction = grammar.constants.Noun.actions[(int) (Math.random() * grammar.constants.Noun.actions.length)];
		
		return randomAction;
			
	}

}

