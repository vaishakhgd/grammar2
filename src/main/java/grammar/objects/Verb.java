package grammar.objects;


import grammar.io.Output;

public class Verb  implements Objects{
	
	
	
	
	public void printOutputLine() {
		System.out.println("----------------------Verb ADDED---------------------------------");
		System.out.println(Output.line);
		System.out.println("---------------------Verb ADDED----------------------------------");
	}
	
	

	@Override
	public String addWordAndReturnAction() {
		
		
		String randomWord = grammar.constants.Verb.words[(int) (Math.random() * grammar.constants.Verb.words.length)];
		
		Output.line += " " + randomWord +" ";
		
		printOutputLine();
		
		String randomAction = grammar.constants.Verb.actions[(int) (Math.random() * grammar.constants.Verb.actions.length)];
		
		return randomAction;
			
	}

}

