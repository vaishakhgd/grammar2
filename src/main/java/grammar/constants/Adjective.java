package grammar.constants;

public class Adjective {
	
	public static final String[] words = {"black","white","dark","light",
			"bright","murky","muddy","clear"};
	
	public static final String[] actions = {"noun","adjective","end"};

}
