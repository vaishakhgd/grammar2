package grammar.constants;

public class Noun {
	public static final String[] words =  {"heart","sun","moon","thunder",
			"fire","time","wind","sea","river","flavor","wave","willow",
			"rain","tree","flower","field","meadow"
			,"pasture","harvest","water","father","mother","brother","sister"};
	
	public static final String[] actions = {"verb","preposition","end"};
}
