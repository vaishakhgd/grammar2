package grammar.constants;

public class Verb {

	public static final String[] words = {"runs","walks","stands","climbs","crawls",
			"flows","flies","transcends","ascends","descends","sinks"};
	
	public static final String[] actions = {"preposition","pronoun","end"};
}
